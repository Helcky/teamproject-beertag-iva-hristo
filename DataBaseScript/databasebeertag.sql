-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table beertag.beers: ~0 rows (approximately)
/*!40000 ALTER TABLE `beers` DISABLE KEYS */;
/*!40000 ALTER TABLE `beers` ENABLE KEYS */;

-- Dumping data for table beertag.beers_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `beers_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `beers_status` ENABLE KEYS */;

-- Dumping data for table beertag.beers_style: ~0 rows (approximately)
/*!40000 ALTER TABLE `beers_style` DISABLE KEYS */;
/*!40000 ALTER TABLE `beers_style` ENABLE KEYS */;

-- Dumping data for table beertag.beers_tags: ~0 rows (approximately)
/*!40000 ALTER TABLE `beers_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `beers_tags` ENABLE KEYS */;

-- Dumping data for table beertag.brewery: ~0 rows (approximately)
/*!40000 ALTER TABLE `brewery` DISABLE KEYS */;
/*!40000 ALTER TABLE `brewery` ENABLE KEYS */;

-- Dumping data for table beertag.country: ~0 rows (approximately)
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
/*!40000 ALTER TABLE `country` ENABLE KEYS */;

-- Dumping data for table beertag.status: ~0 rows (approximately)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

-- Dumping data for table beertag.style: ~0 rows (approximately)
/*!40000 ALTER TABLE `style` DISABLE KEYS */;
/*!40000 ALTER TABLE `style` ENABLE KEYS */;

-- Dumping data for table beertag.tags: ~0 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping data for table beertag.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
