package com.telerikacademy.beertag.service;


import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repository.common.UsersRepository;
import com.telerikacademy.beertag.services.UserServiceImpl;
import com.telerikacademy.beertag.services.common.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createBeer;
import static com.telerikacademy.beertag.Factory.createUser;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UsersRepository repository;

    @InjectMocks
    UserServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenUserExists() {
        User expected = createUser();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        User returnedUser = mockService.getOne(0);

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnUser_whenUserExist() {
        //Arrange
        User expected = createUser();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        //Act
        User returned = mockService.getOne(1);

        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test(expected = ResponseStatusException.class)
    public void getOne_shouldResponseStatusException_whenUserNotExist() {
        //Arrange
        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        User user=mockService.getOne(anyInt());

        //Assert
        Mockito.verify(repository,times(1)).getOne(anyInt());
    }

    @Test
    public void getByUsername_Should_CallRepository_WhenUsernameExist(){

        User expected = createUser();

        Mockito.when(repository.getByUsername(anyString()))
                .thenReturn(expected);

        User returnUsername = mockService.getByUsername("");

        Mockito.verify(repository,Mockito.times(1)).getByUsername(anyString());
    }

    @Test
    public void getByUsername_shouldReturnUsername_whenUserExist() {
        //Arrange
        User expected = createUser();

        Mockito.when(repository.getByUsername(anyString()))
                .thenReturn(expected);

        //Act
        User returned = mockService.getByUsername(anyString());

        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test(expected = ResponseStatusException.class)
    public void getByUsername_shouldResponseStatusException_whenUserNotExist() {
        //Arrange
        Mockito.when(repository.getByUsername(anyString()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        User user=mockService.getByUsername(anyString());

        //Assert
        Mockito.verify(repository,times(1)).getByUsername(anyString());
    }


    @Test
    public void createUser_Should_CallRepository_WhenCreatingUser(){
        //Arrange
        User expected=createUser();


        doNothing().when(repository).create(any(User.class));

        //Act
        mockService.create(expected);

        //Assert
        Mockito.verify(repository,Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUser_Should_ThrowDuplicateException_WhenUserExists(){
        //Arrange
        User expected=createUser();

        Mockito.when(repository.checkUserExists(expected.getUsername())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository,times(1)).getOne(anyInt());

    }

    @Test
    public void getAll_Should_CallRepository(){

        //Arrange
        List<User> expected=new ArrayList<>();
        expected.add(createUser());

        Mockito.when(repository.getAll()).thenReturn(expected);

        //Act
        List<User> returned=mockService.getAll();

        //Assert
        Mockito.verify(repository,times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenUserExists(){
        //Arrange

        User user=createUser();

        //Act
        mockService.delete(user.getId());

        //Assert
        Mockito.verify(repository,times(1)).delete(anyInt());
    }

    @Test
    public void update_Should_CallRepository_WhenUserExists(){
        //Arrange
        User expected=createUser();

        Mockito.when(repository.checkUserExists(expected.getUsername())).thenReturn(true);

        doNothing().when(repository).update(any(User.class));

        //Act
        mockService.update(expected);

        //Assert
        Mockito.verify(repository,times(1)).update(any(User.class));
    }

    @Test
    public void update_Should_ReturnNewUser_WhenOldUserDoesExists(){
        //Arrange
        User expected=createUser();

        Mockito.when(repository.checkUserExists(expected.getUsername())).thenReturn(true);

        //Act
        mockService.update(expected);

        //Assert
        Mockito.verify(repository,times(1)).update(any(User.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_Should_ThrowEntityNotFoundException_WhenUserDoesNotExists(){
        //Arrange
        User user=createUser();

        Mockito.when(repository.checkUserExists(user.getUsername()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        mockService.update(user);

        //Assert
        Mockito.verify(repository,times(1)).update(user);
    }


}
