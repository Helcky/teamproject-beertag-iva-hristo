package com.telerikacademy.beertag.service;


import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repository.common.StyleRepository;
import com.telerikacademy.beertag.services.StyleServiceImpl;
import com.telerikacademy.beertag.services.common.StyleService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createBeer;
import static com.telerikacademy.beertag.Factory.createStyle;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StyleRepository repository;

    @InjectMocks
    StyleServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenStyleExists() {
        Style expected = createStyle();


        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Style returned = mockService.getOne(0);

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnStyle_whenStyleExist() {
        //Arrange
        Style expected = createStyle();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        //Act
        Style returned = mockService.getOne(1);

        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test(expected = ResponseStatusException.class)
    public void getOne_shouldResponseStatusException_whenStyleNotExist() {
        //Arrange
        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        Style style=mockService.getOne(anyInt());

        //Assert
        Mockito.verify(repository,times(1)).getOne(anyInt());
    }

    @Test
    public void getAll_Should_CallRepository(){

        //Arrange
        List<Style> expected=new ArrayList<>();
        expected.add(createStyle());

        Mockito.when(repository.getAll()).thenReturn(expected);

        //Act
        List<Style> returned=mockService.getAll();

        //Assert
        Mockito.verify(repository,times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenStyleExists(){
        //Arrange

        Style style=createStyle();

        //Act
        mockService.delete(style.getID());

        //Assert
        Mockito.verify(repository,times(1)).delete(anyInt());
    }

    @Test
    public void create_Should_CallRepository_WhenCreatingStyle(){
        //Arrange
        Style expected=createStyle();


        doNothing().when(repository).create(any(Style.class));

        //Act
        mockService.create(expected);

        //Assert
        Mockito.verify(repository,Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void create_Should_ThrowDuplicateException_WhenStyleExists(){
        //Arrange
        Style expected=createStyle();

        Mockito.when(repository.checkStyleExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository,times(1)).getOne(anyInt());

    }
}
