package com.telerikacademy.beertag.service;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import com.telerikacademy.beertag.services.BeerServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createBeer;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository repository;

    @InjectMocks
    BeerServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenBeerExists() {
        Beer expected = createBeer();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Beer returnedBeer = mockService.getOne(0);

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnBeer_whenBeerExist() {
        //Arrange
        Beer expectedBeer = createBeer();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = mockService.getOne(1);

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);
    }

    @Test(expected = ResponseStatusException.class)
    public void getOne_shouldResponseStatusException_whenBeerNotExist() {
        //Arrange
        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        Beer beer=mockService.getOne(anyInt());

        //Assert
        Mockito.verify(repository,times(1)).getOne(anyInt());
    }

    @Test
    public void createBeer_Should_CallRepository_WhenCreatingBeer(){
        //Arrange
        Beer expected=createBeer();


        doNothing().when(repository).create(any(Beer.class));

        //Act
        mockService.create(expected);

        //Assert
        Mockito.verify(repository,Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBeer_Should_ThrowDuplicateException_WhenBeerExists(){
        //Arrange
        Beer expected=createBeer();

        Mockito.when(repository.checkBeerExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository,times(1)).getOne(anyInt());

    }

    @Test
    public void getAll_Should_CallRepository(){

        //Arrange
        List<Beer> expected=new ArrayList<>();
        expected.add(createBeer());

        Mockito.when(repository.getAll()).thenReturn(expected);

        //Act
        List<Beer> returned=mockService.getAll();

        //Assert
        Mockito.verify(repository,times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenBeerExists(){
        //Arrange

        Beer beer=createBeer();

        //Act
        mockService.delete(beer.getID());

        //Assert
        Mockito.verify(repository,times(1)).delete(anyInt());
    }


    @Test
    public void update_Should_CallRepository_WhenBeerExists(){
        //Arrange
        Beer expected=createBeer();

        Mockito.when(repository.checkBeerExists(expected.getName())).thenReturn(true);

        doNothing().when(repository).update(any(Beer.class));

        //Act
        mockService.update(expected);

        //Assert
        Mockito.verify(repository,times(1)).update(any(Beer.class));
    }

    @Test
    public void update_Should_ReturnNewBeer_WhenOldBeerDoesExists(){
        //Arrange
        Beer expected=createBeer();

        Mockito.when(repository.checkBeerExists(expected.getName())).thenReturn(true);

        //Act
        mockService.update(expected);

        //Assert
        Mockito.verify(repository,times(1)).update(any(Beer.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_Should_ThrowEntityNotFoundException_WhenBeerDoesNotExists(){
        //Arrange
        Beer beer=createBeer();

        Mockito.when(repository.checkBeerExists(beer.getName()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        mockService.update(beer);

        //Assert
        Mockito.verify(repository,times(1)).update(beer);
    }

    @Test
    public void getByName_Should_CallRepository(){
        //Arrange

        Beer beer=createBeer();

        //Act
        mockService.getByName(beer.getName());

        //Assert
        Mockito.verify(repository,times(1)).getByName(beer.getName());
    }

    @Test
    public void filterByStyle_Should_CallRepository(){
        Style style=new Style("test");

        mockService.filterByStyle(style.getName());

        Mockito.verify(repository,times(1)).filterByStyle(style.getName());

    }

    @Test
    public void filterByCountry_Should_CallRepository(){
        Country country=new Country("test");

        mockService.filterByCountry(country.getName());

        Mockito.verify(repository,times(1)).filterByCountry(country.getName());
    }

    @Test
    public void filterByTag_Should_CallRepository(){
        mockService.filterByTag(anyInt());
        Mockito.verify(repository,times(1)).filterByTag(anyInt());

    }

    @Test
    public void sortByABV_Should_CallRepository(){
        mockService.sortByABV();
        Mockito.verify(repository,times(1)).sortByABV();
    }
    @Test
    public void sortByName_Should_CallRepository(){
        mockService.sortByName();
        Mockito.verify(repository,times(1)).sortByName();
    }
    @Test
    public void sortByRating_Should_CallRepository(){
        mockService.sortByRating();
        Mockito.verify(repository,times(1)).sortByRating();
    }

}
