package com.telerikacademy.beertag.service;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repository.common.CountryRepository;
import com.telerikacademy.beertag.services.CountryServiceImpl;
import com.telerikacademy.beertag.services.common.CountryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createBeer;
import static com.telerikacademy.beertag.Factory.createCountry;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository repository;

    @InjectMocks
    CountryServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenCountryExists() {
        Country expected = createCountry();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Country returned = mockService.getOne(0);

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnCountry_whenCountryExist() {
        //Arrange
        Country expected = createCountry();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        //Act
        Country returned = mockService.getOne(1);

        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test(expected = ResponseStatusException.class)
    public void getOne_Should_ThrowResponseStatusException_whenCountryDoesNotExist(){
        //Arrange
        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        Country country=mockService.getOne(anyInt());

        //Assert
        Mockito.verify(repository,times(1)).getOne(anyInt());
    }


    @Test
    public void getAll_Should_CallRepository(){

        //Arrange
        List<Country> expected=new ArrayList<>();
        expected.add(createCountry());

        Mockito.when(repository.getAll()).thenReturn(expected);

        //Act
        List<Country> returned=mockService.getAll();

        //Assert
        Mockito.verify(repository,times(1)).getAll();
    }

}
