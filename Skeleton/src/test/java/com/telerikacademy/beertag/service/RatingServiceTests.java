package com.telerikacademy.beertag.service;

import com.sun.istack.Interned;
import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.repository.common.RatingRepository;
import com.telerikacademy.beertag.services.RatingServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import static com.telerikacademy.beertag.Factory.createBrewery;
import static com.telerikacademy.beertag.Factory.createRating;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)

public class RatingServiceTests {

    @Mock
    RatingRepository repository;

    @InjectMocks
    RatingServiceImpl mockService;


    @Test
    public void createRating_Should_CallRepository_WhenCreatingRating(){
        //Arrange

        doNothing().when(repository).create(1,2,3);

        //Act
        mockService.create(1,2,3);

        //Assert
        Mockito.verify(repository,Mockito.times(1)).create(1,2,3);
    }

}
