package com.telerikacademy.beertag.service;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repository.common.BreweryRepository;
import com.telerikacademy.beertag.services.BreweryServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.*;
import static com.telerikacademy.beertag.Factory.createBeer;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)

public class BreweryServiceTests {

    @Mock
    BreweryRepository repository;

    @InjectMocks
    BreweryServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenBreweryExists() {
        Brewery expected = createBrewery();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Brewery returned = mockService.getOne(0);

        Mockito.verify(repository, Mockito.times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnCountry_whenBreweryExist() {
        //Arrange
        Brewery expected = createBrewery();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        //Act
        Brewery returned = mockService.getOne(1);

        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test(expected = ResponseStatusException.class)
    public void getOne_Should_ThrowResponseStatusException_whenBreweryDoesNotExist(){
        //Arrange
        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        Brewery brewery=mockService.getOne(anyInt());

        //Assert
        Mockito.verify(repository,times(1)).getOne(anyInt());
    }


    @Test
    public void getAll_Should_CallRepository(){

        //Arrange
        List<Brewery> expected=new ArrayList<>();
        expected.add(createBrewery());

        Mockito.when(repository.getAll()).thenReturn(expected);

        //Act
        List<Brewery> returned=mockService.getAll();

        //Assert
        Mockito.verify(repository,times(1)).getAll();
    }


    @Test
    public void createBrewery_Should_CallRepository_WhenCreatingBrewery(){
        //Arrange
        Brewery expected=createBrewery();


        doNothing().when(repository).create(any(Brewery.class));

        //Act
        mockService.create(expected);

        //Assert
        Mockito.verify(repository,Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBrewery_Should_ThrowDuplicateException_WhenBreweryExists(){
        //Arrange
        Brewery expected=createBrewery();

        Mockito.when(repository.checkBreweryExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository,times(1)).getOne(anyInt());

    }

    @Test
    public void delete_Should_CallRepository_WhenBeerExists(){
        //Arrange

        Brewery beer=createBrewery();

        //Act
        mockService.delete(beer.getID());

        //Assert
        Mockito.verify(repository,times(1)).delete(anyInt());
    }

}
