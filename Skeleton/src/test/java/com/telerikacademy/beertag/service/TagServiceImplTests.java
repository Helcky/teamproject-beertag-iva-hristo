package com.telerikacademy.beertag.service;


import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repository.common.TagRepository;
import com.telerikacademy.beertag.services.TagServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.beertag.Factory.createTag;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository repository;

    @InjectMocks
    TagServiceImpl mockService;

    @Test
    public void getOne_Should_CallRepository_WhenTagExists() {
        Tag expected = createTag();


        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        Tag returned = mockService.getOne(0);

        Mockito.verify(repository, times(1)).getOne(anyInt());
    }

    @Test
    public void getOne_shouldReturnTag_whenTagExist() {
        //Arrange
        Tag expected = createTag();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expected);

        //Act
        Tag returned = mockService.getOne(1);

        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test(expected = ResponseStatusException.class)
    public void getOne_shouldResponseStatusException_whenTagNotExist() {
        //Arrange
        Mockito.when(repository.getOne(anyInt()))
                .thenThrow(new EntityNotFoundException(""));

        //Act
        Tag tag=mockService.getOne(anyInt());

        //Assert
        Mockito.verify(repository,times(1)).getOne(anyInt());
    }

    @Test
    public void getAll_Should_CallRepository(){

        //Arrange
        List<Tag> expected=new ArrayList<>();
        expected.add(createTag());

        Mockito.when(repository.getAll()).thenReturn(expected);

        //Act
        List<Tag> returned=mockService.getAll();

        //Assert
        Mockito.verify(repository,times(1)).getAll();
    }

    @Test
    public void delete_Should_CallRepository_WhenTagExists(){
        //Arrange

        Tag tag=createTag();

        //Act
        mockService.delete(tag.getID());

        //Assert
        Mockito.verify(repository,times(1)).delete(anyInt());
    }

    @Test
    public void create_Should_CallRepository_WhenCreatingStyle(){
        //Arrange
        Tag expected=createTag();


        doNothing().when(repository).create(any(Tag.class));

        //Act
        mockService.create(expected);

        //Assert
        Mockito.verify(repository,Mockito.times(1)).create(expected);
    }

    @Test(expected = DuplicateEntityException.class)
    public void create_Should_ThrowDuplicateException_WhenStyleExists(){
        //Arrange
        Tag expected=createTag();

        Mockito.when(repository.checkTagExists(expected.getName())).
                thenReturn(true);

        mockService.create(expected);

        Mockito.verify(repository,times(1)).getOne(anyInt());

    }
}
