package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import com.telerikacademy.beertag.repository.common.RatingRepository;
import com.telerikacademy.beertag.repository.common.UsersRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class UserRepositoryImpl implements UsersRepository {

    private SessionFactory sessionFactory;
    private BeerRepository beerRepository;
    private RatingRepository ratingRepository;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, BeerRepository beerRepository,RatingRepository ratingRepository) {
        this.sessionFactory = sessionFactory;
        this.beerRepository = beerRepository;
        this.ratingRepository=ratingRepository;
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException(
                        String.format("User with id: %d does not exist", id)
                );
            }
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from User", User.class)
                    .list();
        }
    }

    @Override
    public void update(User newUser) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(newUser);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkUserExists(String name) {
        return getByUsername(name) != null;
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username",   username);
            List<User> users = query.list();
            if (users.size() != 1){
                throw new EntityNotFoundException(
                        String.format("User with username:%s does not exist",username)
                );
            }
            return users.get(0);
        }
    }

    public User getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException(
                        String.format("User with id:%d does not exist.", id));
            }
            return user;
        }
    }


    public void removeFromList(String listOfChoice, int user_id, int beer_id) {
        try (Session session = sessionFactory.openSession()) {

            if (listOfChoice.equalsIgnoreCase("wishlist")) {
                removeFromWishList(user_id, beer_id);

            } else if (listOfChoice.equalsIgnoreCase("dranklist")) {
                removeFromDrankList(user_id, beer_id);
            } else {

                throw new IllegalArgumentException(String.format("%s does not exist for this user", listOfChoice));
            }
        }
    }

    public Set<Beer> showList(String listOfChoice,int user_id){

        try(Session session=sessionFactory.openSession()) {
            if (listOfChoice.equalsIgnoreCase("wishlist")) {
                return showWishList(user_id);
            } else if (listOfChoice.equalsIgnoreCase("dranklist")) {
                return showDrankList(user_id);
            }else{
                throw new IllegalArgumentException(String.format("'%s' does not exist for users.",listOfChoice));
            }
        }
    }

    public boolean checkIfBeerExistInWishList(int user_id, int beer_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery("select user_id, beer_id from wishlist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id",user_id);
            query.setParameter("beer_id",beer_id);
            return query.list().size() != 0;
        }
    }

    public boolean checkIfBeerExistInDrankList(int user_id, int beer_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery("select user_id, beer_id " +
                    "from dranklist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id",user_id);
            query.setParameter("beer_id",beer_id);
            return query.list().size() != 0;
        }
    }

    public boolean checkIfBeerIsRated(int user_id,int beer_id){
        try(Session session=sessionFactory.openSession()){
            Query query = session.createSQLQuery("select user_id, beer_id,rating " +
                    "from rating where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id",user_id);
            query.setParameter("beer_id",beer_id);
            return query.list().size()!=0;
        }
    }

    public Object getRateForBeer(int user_id,int beer_id,int rating){
        try(Session session=sessionFactory.openSession()){
            Query query = session.createSQLQuery("select user_id, beer_id,rating " +
                    "from rating where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id",user_id);
            query.setParameter("beer_id",beer_id);
            return  query.getSingleResult();
        }
    }

    public void removeRateForBeer(int user_id,int beer_id){
        try(Session session=sessionFactory.openSession()) {

            session.beginTransaction();
            Query query = session.createSQLQuery("delete from rating where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

    public double ratingSumForBeer(int beer_id){
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            Query query = session.createSQLQuery("select avg(rating) from rating b where b.beer_id=:beer_id");
            query.setParameter("beer_id",beer_id);
            session.getTransaction().commit();
            return (Double) query.getSingleResult();
        }
    }

    private Set<Beer> showWishList(int user_id){
        User user=getOne(user_id);
        return user.getWishList();
    }

    private Set<Beer> showDrankList(int user_id){
        User user=getOne(user_id);
        return user.getDranklist();
    }

    private void removeFromWishList(int user_id,int beer_id){
        try(Session session=sessionFactory.openSession()) {
            User user = session.get(User.class,user_id);
            Beer beer = beerRepository.getOne(beer_id);

            session.beginTransaction();
            user.removeFromWishlist(beer);
            Query query = session.createSQLQuery("delete from wishlist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

    private void removeFromDrankList(int user_id,int beer_id){
        try(Session session=sessionFactory.openSession()){
            User user = session.get(User.class,user_id);
            Beer beer = beerRepository.getOne(beer_id);

            session.beginTransaction();
            user.removeFromDranklist(beer);
            Query query = session.createSQLQuery("delete from dranklist where user_id=:user_id and beer_id=:beer_id");
            query.setParameter("user_id", user_id);
            query.setParameter("beer_id", beer_id);
            query.executeUpdate();
            session.getTransaction().commit();
        }

    }





}
