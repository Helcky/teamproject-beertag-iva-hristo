package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repository.common.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Tag> getAll() {

        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Tag", Tag.class)
                    .list();
        }
    }

    public Tag getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException(String.format("Tag with id:%d does not exists", id));
            }
            return tag;
        }
    }

    public List<Tag> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name LIKE :name", Tag.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    public void create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
    }

    public void delete(int id) {

        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException(String.format("Tag with id:%d does not exists.", id));
            }
            session.delete(tag);
        }
    }

    public boolean checkTagExists(String name) {
        return getByName(name).size() != 0;
    }

}
