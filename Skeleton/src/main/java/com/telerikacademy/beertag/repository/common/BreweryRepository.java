package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {
    void create(Brewery brewery);
    void delete(int id);
    Brewery getOne(int id);
    List<Brewery> getAll();
    boolean checkBreweryExists(String name);
}
