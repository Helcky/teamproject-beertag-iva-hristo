package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Country;
import com.telerikacademy.beertag.repository.common.CountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

@Repository
public class CountryRepositoryImpl implements CountryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public List<Country> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Country", Country.class)
                    .list();

        }
    }

    public Country getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (country == null) {
                throw new EntityNotFoundException(String.format("Country with id: %d does not exist", id));
            }
            return country;
        }
    }

    public List<Country> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name LIKE :name", Country.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }


    public boolean checkCountryExists(String name) {
        return getByName(name).size() != 0;
    }

}
