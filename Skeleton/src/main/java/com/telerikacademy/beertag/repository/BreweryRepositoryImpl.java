package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.repository.common.BreweryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null) {
                throw new EntityNotFoundException(String.format("Brewery with id: %d does not exist", id));
            }
            session.delete(brewery);
        }
    }

    @Override
    public Brewery getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null) {
                throw new EntityNotFoundException(String.format("Brewery with id: %d does not exist", id));
            }
            return brewery;
        }
    }

    public List<Brewery> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name LIKE :name", Brewery.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Brewery", Brewery.class)
                    .list();
        }
    }

    public boolean checkBreweryExists(String name) {
        return getByName(name).size() != 0;
    }

    //TODO update method

}
