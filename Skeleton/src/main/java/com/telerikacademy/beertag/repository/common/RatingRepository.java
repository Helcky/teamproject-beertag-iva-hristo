package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Rating;

import java.util.List;

public interface RatingRepository {
    void create(int user_id,int beer_id,int rating);
}
