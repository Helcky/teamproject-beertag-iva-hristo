package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repository.common.StyleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Style> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Style", Style.class)
                    .list();
        }
    }

    public Style getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException(String.format("Style with id: %d does not exists", id));
            }
            return style;
        }
    }


    public void create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
    }

    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException(String.format("Style with id: %d does not exists", id));
            }
            session.delete(style);
        }
    }

    public boolean checkStyleExists(String name) {
        return getByName(name).size() != 0;
    }

    public List<Style> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name LIKE :name", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

}
