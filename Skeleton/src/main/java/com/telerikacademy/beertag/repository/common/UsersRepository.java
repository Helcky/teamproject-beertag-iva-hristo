package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.User;

import java.util.List;
import java.util.Set;

public interface UsersRepository {

    void create(User user);
    List<User> getAll();
    void delete (int id);
    void update(User user);
    boolean checkUserExists(String name);
    User getByUsername(String username);
    User getOne(int id);
    void removeFromList(String list,int user_id,int beer_id);
     boolean checkIfBeerExistInWishList(int user_id, int beer_id);
     boolean checkIfBeerExistInDrankList(int user_id, int beer_id);
    Set<Beer> showList(String listOfChoice, int user_id);
    void removeRateForBeer(int user_id,int beer_id);
    boolean checkIfBeerIsRated(int user_id,int beer_id);
//    double ratingSumForBeer(int beer_id);
    Object getRateForBeer(int user_id, int beer_id, int rating);
    double ratingSumForBeer(int beer_id);
//    int showUserNum();
//    double ratingSumForBeer(int beer_id);

}
