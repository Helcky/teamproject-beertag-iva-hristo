package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();
    List<Country> getByName(String name);
    Country getOne(int id);
    boolean checkCountryExists(String name);
}
