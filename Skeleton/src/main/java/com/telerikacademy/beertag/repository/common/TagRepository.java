package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagRepository {

    void create(Tag brewery);
    void delete(int id);
    Tag getOne(int id);
    List<Tag> getAll();
    List<Tag> getByName(String name);
    boolean checkTagExists(String name);

}
