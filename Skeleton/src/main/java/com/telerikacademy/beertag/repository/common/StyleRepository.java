package com.telerikacademy.beertag.repository.common;

import com.telerikacademy.beertag.models.Style;

import java.util.List;

public interface StyleRepository {
    void create(Style style);
    void delete(int id);
    Style getOne(int id);
    List<Style> getAll();
    boolean checkStyleExists(String name);
}
