package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repository.common.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.action.internal.EntityActionVetoException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory=sessionFactory;
    }

    @Override
    public void create(int user_id,int beer_id,int rating) {
        try(Session session=sessionFactory.openSession()){
            User user=session.get(User.class,user_id);
            Beer beer=session.get(Beer.class,beer_id);

            if(user== null || beer==null){
                throw new EntityNotFoundException(
                        String.format("Beer with id:%d or user with id:%d not exist",beer_id,user_id)
                );
            }
            session.beginTransaction();
            Rating newRating=new Rating(user,beer,rating);
            session.save(newRating);
            session.getTransaction().commit();
        }
    }

}
