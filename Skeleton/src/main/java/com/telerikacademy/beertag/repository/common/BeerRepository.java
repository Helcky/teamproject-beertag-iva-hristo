package com.telerikacademy.beertag.repository.common;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface BeerRepository {

    void create(Beer beer);
    List<Beer> getAll();
    Beer getOne(int id);
    void delete(int id);
    void update(Beer beer);
    boolean checkBeerExists(String name);
    List<Beer> filterByStyle(String name);
    List<Beer> filterByTag(int tag_id);
    List<Beer> filterByCountry(String name);
    List<Beer> sortByABV();
    List<Beer> sortByName();
    List<Beer> sortByRating();
    boolean checkIfTagIsAlreadyInList(int beer_id,int tag_id);
    List<Beer> getByName(String name);
    List<Rating> getAllRatingForBeer(int beer_id);

}
