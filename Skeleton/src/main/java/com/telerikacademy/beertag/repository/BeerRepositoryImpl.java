package com.telerikacademy.beertag.repository;

import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
    }

    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Beer", Beer.class).list();
        }
    }

    @Override
    public Beer getOne(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException(
                        String.format("Beer with id:%d does not exist.", id));
            }
            return beer;
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException(
                        String.format("Beer with id:%d does not exist.", id));
            }
            session.beginTransaction();
            session.delete(beer);
            session.getTransaction().commit();
        }
    }


    public void update(Beer newBeer) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(newBeer);
            session.getTransaction().commit();
        }
    }

    public boolean checkBeerExists(String name) {
        return getByName(name).size() != 0;
    }


    public List<Beer> filterByStyle(String name) {
        try (Session session = sessionFactory.openSession()) {
            Style style = getStyleIfExist(name);

            session.beginTransaction();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> criteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> root = criteriaQuery.from(Beer.class);
            criteriaQuery.where(criteriaBuilder.equal(root.get("style"), style));

            Query<Beer> query = session.createQuery(criteriaQuery);
            List<Beer> result = query.getResultList();
            session.getTransaction().commit();

            return result;
        }
    }

    public List<Beer> filterByTag(int tag_id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = getTagIfExist(tag_id);

            session.beginTransaction();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> beerCriteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> beerRoot = beerCriteriaQuery.from(Beer.class);
            beerCriteriaQuery.select(beerRoot);
            Predicate predicate = criteriaBuilder.equal(beerRoot.get("tags"), "#beer");
            beerCriteriaQuery.where(predicate);
            Query<Beer> beerQuery = session.createQuery(beerCriteriaQuery);
            List<Beer> allBeers = beerQuery.getResultList();

            List<Beer> result=new ArrayList<>();

            for (Beer beer: allBeers) {
                if(beer.getTags().contains(tag)){
                    result.add(beer);
                }
            }
            session.getTransaction().commit();
            return result;
        }
    }

    public List<Beer> filterByCountry(String name) { //int country_id
        try (Session session = sessionFactory.openSession()) {

//            Country country = getCountryIfExist(country_id);
            Country country=getCountryIfExist(name);

            session.beginTransaction();
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Brewery> criteriaQuery = criteriaBuilder.createQuery(Brewery.class);
            Root<Brewery> breweryRoot = criteriaQuery.from(Brewery.class);
            criteriaQuery.where(criteriaBuilder.equal(breweryRoot.get("country"), country));
            Query<Brewery> query = session.createQuery(criteriaQuery);
            List<Brewery> breweries = query.getResultList();


            CriteriaQuery<Beer> beerCriteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> beerRoot = beerCriteriaQuery.from(Beer.class);
            beerCriteriaQuery.select(beerRoot);
            Query<Beer> beerQuery = session.createQuery(beerCriteriaQuery);
            List<Beer> allBeers = beerQuery.getResultList();

            List<Beer> result = new ArrayList<>();

            for (Brewery brewery : breweries) {
                for (Beer beer : allBeers) {
                    if (beer.getBrewery().equals(brewery)) {
                        result.add(beer);
                    }
                }
            }
            session.getTransaction().commit();

            return result;
        }
    }

    public List<Beer> sortByABV() {
        try (Session session = sessionFactory.openSession()) {
            return sortBy("abv");
        }
    }

    public List<Beer> sortByName() {
        try (Session session = sessionFactory.openSession()) {
            return sortBy("name");
        }
    }

    public List<Beer> sortByRating(){
        try (Session session=sessionFactory.openSession()){
            return sortBy("rating");
        }

    }

    //TODO sort by rating

    private List<Beer> sortBy(String criteria) {
        try (Session session = sessionFactory.openSession()) {

            session.beginTransaction();

            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> criteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> root = criteriaQuery.from(Beer.class);

            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(criteria)));

            Query<Beer> query = session.createQuery(criteriaQuery);
            List<Beer> result = query.getResultList();

            session.getTransaction().commit();

            return result;
        }
    }

    public boolean checkIfTagIsAlreadyInList(int beer_id,int tag_id){
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createSQLQuery("select beer_id, tag_id from beers_tags where beer_id=:beer_id and tag_id=:tag_id");
            query.setParameter("beer_id",beer_id);
            query.setParameter("tag_id",tag_id);
            return query.list().size() != 0;
        }
    }

    private Style getStyleIfExist(String name) {
//        try (Session session = sessionFactory.openSession()) {
//            Style style = session.get(Style.class, style_id);
//            if (style == null) {
//                throw new EntityNotFoundException(String.format("Style with id:%d does not exist.", style_id));
//            }
//
//            return style;
//        }
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name LIKE :name", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.getSingleResult();
        }
    }

    private Tag getTagIfExist(int tag_id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, tag_id);

            if (tag == null) {
                throw new EntityNotFoundException(String.format("Tag with id:%d does not exist", tag_id));
            }
            return tag;
        }
    }

    private Country getCountryIfExist(String name) {
//        try (Session session = sessionFactory.openSession()) {
//            Country country = session.get(Country.class, country_id);
//
//            if (country == null) {
//                throw new EntityNotFoundException(String.format("Country with id:%d does not exist", country_id));
//            }
//            return country;
//        }
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name LIKE :name", Country.class);
            query.setParameter("name", "%" + name + "%");
            return query.getSingleResult();
        }
    }

    public List<Beer> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name LIKE :name", Beer.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    public List<Rating> getAllRatingForBeer(int beer_id){
        try(Session session=sessionFactory.openSession()){
            NativeQuery query = session.createSQLQuery("select beer_id from rating where beer_id=:beer_id");
            query.setParameter("beer_id",beer_id);
            return query.list();
        }
    }
}
