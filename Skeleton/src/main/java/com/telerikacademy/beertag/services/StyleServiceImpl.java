package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Style;
import com.telerikacademy.beertag.repository.common.StyleRepository;
import com.telerikacademy.beertag.services.common.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {

    private StyleRepository repository;

    @Autowired
    public StyleServiceImpl(StyleRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Style style) {
        if (repository.checkStyleExists(style.getName())) {
            throw new DuplicateEntityException(
                    String.format("Style with name:%s already exists", style.getName()));
        }
        repository.create(style);
    }

    @Override
    public void delete(int id) {
        try {
            repository.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public Style getOne(int id) {
        try {
            return repository.getOne(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public List<Style> getAll() {
        return repository.getAll();
    }
}
