package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.User;

import java.util.List;
import java.util.Set;

public interface UserService {
    void create(User user);

    List<User> getAll();

    void update(User user);

    void delete(int id);

    User getByUsername(String username);

    User getOne(int id);

    void addToList(String listOfChoice,int user_id,int beer_id);

    void removeFromList(String listOfChoice,int user_id,int beer_id);

    Set<Beer> showList(String listOfChoice, int user_id);

    void rateBeer(int user_id,int beer_id,int rating);

}
