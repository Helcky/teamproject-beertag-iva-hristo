package com.telerikacademy.beertag.services;

public class ServiceConstants {

     static final String NOT_EXISTING_USER="User with id:%d does not exist.";
     static final String NOT_EXISTING_BEER="Beer with id:%d does not exist.";
     static final String BEER_ALREADY_EXIST_IN_WISHLIST="Beer with id:%d already exist in wishlist of user with id:%d";
     static final String BEER_ALREADY_EXIST_IN_DRANKLIST="Beer with id:%d already exist in dranklist of user with id:%d";
     static final String NOT_EXISTING_LIST="%s does not exist for this user";
     static final String NOT_EXISTING_TAG="Tag with id:%d does not exist";
     static final String TAG_ALREADY_EXIST_IN_LIST_OF_TAGS="Tag with id:%d alredy exist in list of beer with id:%d";
}
