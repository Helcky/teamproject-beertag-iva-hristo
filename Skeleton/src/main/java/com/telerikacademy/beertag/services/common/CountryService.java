package com.telerikacademy.beertag.services.common;


import com.telerikacademy.beertag.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();
    Country getOne(int id);
}
