package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Style;

import java.util.List;

public interface StyleService {
    void create(Style style);
    void delete(int id);
    Style getOne(int id);
    List<Style> getAll();
}
