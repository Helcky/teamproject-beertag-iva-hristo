package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;

import java.util.List;

public interface BeerService {
    void create(Beer beer);
    List<Beer> getAll();
    Beer getOne(int id);
    void delete(int id);
    void update(Beer beer);
    List<Beer> filterByStyle(String name);
    List<Beer> filterByTag(int tag_id);
    List<Beer> filterByCountry(String name);
    List<Beer> sortByABV();
    List<Beer> sortByName();
    List<Beer> sortByRating();
    void addTagToBeer(int beer_id, int tag_id);
    List<Beer> getByName(String name);
    List<Rating> getAllRatingForBeer(int beer_id);
  //  void addRating(int beer_id,int rating);
}
