package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Rating;

import java.util.List;

public interface RatingService {
   void create(int user_id,int beer_id,int rating);
}
