package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.repository.common.BreweryRepository;
import com.telerikacademy.beertag.services.common.BreweryService;
import org.hibernate.action.internal.EntityActionVetoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {

    private BreweryRepository repository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Brewery brewery) {
        if (repository.checkBreweryExists(brewery.getName())) {
            throw new DuplicateEntityException(
                    String.format("Brewery with id:%d already exists", brewery.getID()));
        }
        repository.create(brewery);
    }

    @Override
    public void delete(int id) {
        try {
            repository.delete(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public Brewery getOne(int id) {
        try{
        return repository.getOne(id);}
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public List<Brewery> getAll() {
        return repository.getAll();
    }
}
