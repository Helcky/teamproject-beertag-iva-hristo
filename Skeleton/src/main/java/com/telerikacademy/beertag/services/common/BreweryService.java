package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {
    void create(Brewery brewery);
    void delete(int id);
    Brewery getOne(int id);
    List<Brewery> getAll();
}
