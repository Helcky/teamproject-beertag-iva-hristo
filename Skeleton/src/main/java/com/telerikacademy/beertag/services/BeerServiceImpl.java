package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import com.telerikacademy.beertag.repository.common.TagRepository;
import com.telerikacademy.beertag.services.common.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class BeerServiceImpl implements BeerService {

    private BeerRepository beerRepository;
    private TagRepository tagRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository repository,TagRepository tagRepository) {
        this.beerRepository = repository;
        this.tagRepository=tagRepository;
    }

    @Override
    public void create(Beer beer) {
        if (beerRepository.checkBeerExists(beer.getName())) {
            throw new DuplicateEntityException(
                    String.format("Beer with name: %s already exist", beer.getName())
            );
        }
        beerRepository.create(beer);
    }

    @Override
    public List<Beer> getAll() {
        return beerRepository.getAll();
    }

    @Override
    public Beer getOne(int id) {
        try {
            return beerRepository.getOne(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    public List<Beer> getByName(String name){
        return beerRepository.getByName(name);
    }

    @Override
    public void delete(int id) {
        try {
            beerRepository.delete(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public void update(Beer beer) {

        if (!beerRepository.checkBeerExists(beer.getName())) {
            throw new EntityNotFoundException(
                    String.format("Beer with name:%s does not exists", beer.getName())
            );
        }
        beerRepository.update(beer);
    }

    public List<Beer> filterByStyle(String name){
        return beerRepository.filterByStyle(name);
    }

    public List<Beer> filterByTag(int tag_id){

        return beerRepository.filterByTag(tag_id);
    }

    public List<Beer> filterByCountry(String name){
        return beerRepository.filterByCountry(name);
    }

    public List<Beer> sortByABV(){
        return beerRepository.sortByABV();
    }

    public List<Beer> sortByName(){
        return beerRepository.sortByName();
    }

    public List<Beer> sortByRating(){return beerRepository.sortByRating();}

    public void addTagToBeer(int beer_id,int tag_id){
        Beer beer=getBeer(beer_id);
        Tag tag=getTag(tag_id);
        if(beerRepository.checkIfTagIsAlreadyInList(beer_id, tag_id)){
            throw new DuplicateEntityException(
                    String.format(TAG_ALREADY_EXIST_IN_LIST_OF_TAGS,tag_id,beer_id)
            );
        }
        beer.addTag(tag);
        beerRepository.update(beer);
    }

    public List<Rating> getAllRatingForBeer(int beer_id){
        return beerRepository.getAllRatingForBeer(beer_id);
    }

    private Beer getBeer(int beer_id) {
        Beer beer = beerRepository.getOne(beer_id);
        if (beer == null) {
            throw new EntityNotFoundException(
                    String.format(NOT_EXISTING_BEER, beer_id));
        }
        return beer;
    }

    private Tag getTag(int tag_id){
        Tag tag=tagRepository.getOne(tag_id);
        if(tag==null){
            throw new EntityNotFoundException(String.format(NOT_EXISTING_TAG,tag_id));
        }
        return tag;
    }

}
