package com.telerikacademy.beertag.services.common;

import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagService {

    void create(Tag tag);
    void delete(int id);
    Tag getOne(int id);
    List<Tag> getAll();
}
