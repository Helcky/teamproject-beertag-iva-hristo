package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repository.common.TagRepository;
import com.telerikacademy.beertag.services.common.TagService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private TagRepository repository;

    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    public List<Tag> getAll() {
        return repository.getAll();
    }

    public Tag getOne(int id) {
        try {
            return repository.getOne(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    public void create(Tag tag) {
        if (repository.checkTagExists(tag.getName())) {
            throw new DuplicateEntityException(
                    String.format("Tag with name:%s already exists", tag.getName()));
        }
        repository.create(tag);
    }

    public void delete(int id) {
        repository.delete(id);
    }


}
