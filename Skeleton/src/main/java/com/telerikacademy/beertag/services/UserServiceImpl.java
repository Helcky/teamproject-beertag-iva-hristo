package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Rating;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repository.common.BeerRepository;
import com.telerikacademy.beertag.repository.common.RatingRepository;
import com.telerikacademy.beertag.repository.common.UsersRepository;
import com.telerikacademy.beertag.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Set;

import static com.telerikacademy.beertag.services.ServiceConstants.*;

@Service
public class UserServiceImpl implements UserService {

    private UsersRepository repository;
    private BeerRepository beerRepository;
    private RatingRepository ratingRepository;

    @Autowired
    public UserServiceImpl(UsersRepository repository, BeerRepository beerRepository,RatingRepository ratingRepository) {
        this.beerRepository = beerRepository;
        this.repository = repository;
        this.ratingRepository=ratingRepository;
    }

    @Override
    public void create(User user) {
        if (repository.checkUserExists(user.getUsername())) {
            throw new DuplicateEntityException(
                    String.format("User with name %s already exists", user.getUsername())
            );
        }
        repository.create(user);
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

        //TODO getById

        @Override
        public void update(User user) {
            if (!repository.checkUserExists(user.getUsername())) {
                throw new EntityNotFoundException(
                        String.format("User with name:%s does not exist", user.getUsername())
                );
            }
            repository.update(user);
        }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public User getByUsername(String username) {
        try {
            return repository.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    public User getOne(int id) {
        try {
            return repository.getOne(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    public void addToList(String listOfChoice, int user_id, int beer_id) {
        User user = repository.getOne(user_id);

        if (listOfChoice.equalsIgnoreCase("wishlist")) {
            addToWishList(user_id, beer_id);

        } else if (listOfChoice.equalsIgnoreCase("dranklist")) {

            addToDrankList(user_id, beer_id);
        } else {

            throw new IllegalArgumentException(String.format(NOT_EXISTING_LIST, listOfChoice));
        }
    }

    public void removeFromList(String list, int user_id, int beer_id) {
        repository.removeFromList(list, user_id, beer_id);
    }

    public Set<Beer> showList(String listOfChoice,int user_id) {
       return repository.showList(listOfChoice,user_id);
    }

    public void rateBeer(int user_id,int beer_id,int rating){

        User user=repository.getOne(user_id);
        Beer beer=beerRepository.getOne(beer_id);

        if(repository.checkIfBeerIsRated(user_id,beer_id)){
            repository.removeRateForBeer(user_id, beer_id);
        }

        ratingRepository.create(user_id, beer_id, rating);
    }

    private void addToWishList(int user_id, int beer_id) {
        User user = repository.getOne(user_id);
        Beer beer = beerRepository.getOne(beer_id);
        if (repository.checkIfBeerExistInWishList(user_id, beer_id)) {
            throw new DuplicateEntityException(
                    String.format(BEER_ALREADY_EXIST_IN_WISHLIST, beer_id, user_id));
        }
        user.addToWishlist(beer);
        repository.update(user);
    }

    private void addToDrankList(int user_id, int beer_id) {
        User user = repository.getOne(user_id);
        Beer beer = beerRepository.getOne(beer_id);
        if (repository.checkIfBeerExistInDrankList(user_id, beer_id)) {
            throw new DuplicateEntityException(
                    String.format(BEER_ALREADY_EXIST_IN_DRANKLIST, beer_id, user_id));
        }
        user.addToDranklist(beer);
        repository.update(user);
    }




}
