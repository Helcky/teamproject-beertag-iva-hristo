package com.telerikacademy.beertag.controller;


import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.services.common.*;
import org.cloudinary.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class BeerController {

    private BeerService beerService;
    private StyleService styleService;
    private BreweryService breweryService;
    private TagService tagService;
    private CountryService countryService;
    private UserService userService;

    @Autowired
    @Qualifier("com.cloudinary.cloud_name")
    String mCloudName;

    @Autowired
    @Qualifier("com.cloudinary.api_key")
    String mApiKey;

    @Autowired
    @Qualifier("com.cloudinary.api_secret")
    String mApiSecret;


    @Autowired
    public BeerController(BeerService beerService,StyleService styleService,BreweryService breweryService,TagService tagService,
                          CountryService countryService,UserService userService){
        this.beerService=beerService;
        this.styleService=styleService;
        this.breweryService=breweryService;
        this.tagService=tagService;
        this.countryService=countryService;
        this.userService=userService;
    }

    @GetMapping("/menu.html")
    public String showBeers(Model model){
        model.addAttribute("beers",beerService.getAll());
        return "menu";
    }

    @GetMapping("/getBeer/{id}")
    public String getOneBeer(@PathVariable int id, Model model){
        model.addAttribute("beer",beerService.getOne(id));

        return "beerProfile";
    }

    @GetMapping("/filterByStyle.html")
    public String showBeersInStyleFilter(Model model){
        model.addAttribute("beers",beerService.getAll());
        return "filterByStyle";
    }

    @RequestMapping("/add/wishlist/{beer_id}")
    public String addBeerToWishList(Model model,@PathVariable int beer_id,Principal principal){
       User user = userService.getByUsername(principal.getName());
       try {
           userService.addToList("wishlist", user.getId(), beer_id);
       }catch (DuplicateEntityException d){
           return "error-page-beers-menus";
       }
        model.addAttribute("beer",beerService.getOne(beer_id));
        return "beerProfile";
    }

    @RequestMapping("/add/dranklist/{beer_id}")
    public String addBeerToDrankList(Model model,@PathVariable int beer_id,Principal principal){
        User user = userService.getByUsername(principal.getName());
        try {
            userService.addToList("dranklist", user.getId(), beer_id);
        }catch (DuplicateEntityException d){
            return "error-page-beers-menus";
        }
        model.addAttribute("beer",beerService.getOne(beer_id));
        return "beerProfile";
    }

    @RequestMapping("/search/style")
    public String filterByStyle(String style,Model model){
        model.addAttribute("beers",beerService.filterByStyle(style));
        return "filterByStyle";
    }

    @GetMapping("/filterByCountry.html")
    public String showBeersInCountryFilter(Model model){
        model.addAttribute("beers",beerService.getAll());
        return "filterByCountry";
    }

    @RequestMapping("/search/country")
    public String filterByCountry(String country,Model model){
        model.addAttribute("beers",beerService.filterByCountry(country));
        return "filterByCountry";
    }


    @GetMapping("sortedByABV.html")
    public String sortByABV(Model model){
        model.addAttribute("beers",beerService.sortByABV());
        return "sortedByABV";
    }

    @GetMapping("sortedByName.html")
    public String sortByName(Model model){
        model.addAttribute("beers",beerService.sortByName());
        return "sortedByName";
    }

    @GetMapping("sortedByRating.html")
    public String sortByRating(Model model){
        model.addAttribute("beers",beerService.sortByRating());
        return "sortedByRating";
    }


    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model){
        model.addAttribute("beer",new BeerDTO());
        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("breweries",breweryService.getAll());
        model.addAttribute("tags",tagService.getAll());
        return "beer";
    }

    @GetMapping("/country")
    public String showCountryForm(Model model){
        model.addAttribute("countries",countryService.getAll());
        return "country";
    }

    @PostMapping("/beers/new")
    public String createBeer(@Valid @ModelAttribute("beer") BeerDTO beer, @RequestParam MultipartFile aFile,Model model){
        Beer newBeer=mapperCreate(beer);
        newBeer.setBeerPicture(getPictureURL(aFile));
        beerService.create(newBeer);
        model.addAttribute("beers",beerService.getAll());
        return "menu";
    }

    @GetMapping("/beer/name")
    public String getByName(@ModelAttribute("name") String name,Model model){
        model.addAttribute("name",beerService.getByName(name));
        return "beers";
    }

    @RequestMapping("/search")
    public String getBeers(String beerName,Model model) {
//        List<String> options = new ArrayList<>();
//        options.add("name");
        model.addAttribute("beers", beerService.getByName(beerName));
        //        model.addAttribute("options", options);
//        model.addAttribute("countries", countryService.getAll());
//        model.addAttribute("breweries", breweryService.getAll());
//        model.addAttribute("styles", styleService.getAll());
        return "menu";
    }


//    @RequestMapping("/search/country")
//    public String filteryCoutry(String country,Model model){
//        model.addAttribute("beers",beerService.filterByCountry(country));
//        return "filterByCountry.html";
//    }

    private Beer mapperCreate(BeerDTO beerDTO){
        try{
            Beer newBeer=new Beer();
            return mainMapper(newBeer,beerDTO);
            //TODO tags
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private Beer mainMapper(Beer newBeer, BeerDTO beerDTO) {
        try {
            newBeer.setName(beerDTO.getName());
            newBeer.setDescription(beerDTO.getDescription());
            newBeer.setAbv(beerDTO.getAbv());
            Style style = styleService.getOne(beerDTO.getStyleID());
            newBeer.setStyle(style);
            Brewery brewery = breweryService.getOne(beerDTO.getBreweryID());
            newBeer.setBrewery(brewery);
            Tag tag=tagService.getOne(beerDTO.getTagID());
            List<Tag> tags=new ArrayList<>();
            tags.add(tag);
            newBeer.setTags(tags);
            return newBeer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/edit-beer/{id}")
    public String getEditBeer(@PathVariable int id, Model model){
        Beer beer = beerService.getOne(id);

        BeerDTO beerDTO = new BeerDTO();
        beerDTO.setName(beer.getName());
        beerDTO.setDescription(beer.getDescription());
        beerDTO.setAbv(beer.getAbv());
        beerDTO.setBreweryID(beer.getBrewery().getID());
        beerDTO.setStyleID(beer.getStyle().getID());

        model.addAttribute("style", styleService.getAll());
        model.addAttribute("beerDTO", beerDTO);
        model.addAttribute("beer",beer);

        return "edit-beer";
    }

    @PostMapping("/edit-beer/{id}")
    public String putEditBeer(@PathVariable("id") int id, @Valid @ModelAttribute("beer") Beer beer,
                              BindingResult errors,
                              @RequestParam MultipartFile aFile){
        Beer toUpdate = beerService.getOne(id);

        toUpdate.setName(beer.getName());
        toUpdate.setDescription(beer.getDescription());
        toUpdate.setAbv(beer.getAbv());
//        toUpdate.setStyleID(beer.getStyle().getID());
//        toUpdate.setBreweryID(beer.getBrewery().getID());
//        toUpdate.setTagID(beer.getTags().get(0));

        try {
            toUpdate.setBeerPicture(getPictureURL(aFile));
        } catch (Exception ignored){

        }

        beerService.update(toUpdate);
        return "redirect:/";
    }

    private String getPictureURL(MultipartFile aFile) {
        Cloudinary c = new Cloudinary("cloudinary://" + mApiKey + ":" + mApiSecret + "@" + mCloudName);
        String url = "";
        try {
            File f = Files.createTempFile("temp", aFile.getOriginalFilename()).toFile();
            aFile.transferTo(f);

            Map response = c.uploader().upload(f, ObjectUtils.emptyMap());
            JSONObject json = new JSONObject(response);
            url = json.getString("url");
            url = url.substring(0, 49) + "w_500,h_500,c_fill/" + url.substring(49);

        } catch (Exception e) {
            throw new EntityNotFoundException("Picture not found!");
        }
        return url;
    }

}
