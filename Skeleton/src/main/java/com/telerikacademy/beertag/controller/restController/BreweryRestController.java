package com.telerikacademy.beertag.controller.restController;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.Brewery;
import com.telerikacademy.beertag.services.common.BreweryService;
import com.telerikacademy.beertag.services.common.CountryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {

    private BreweryService service;
    private CountryService countryService;

    public BreweryRestController(BreweryService service){
        this.service=service;
    }

    @GetMapping
    public List<Brewery> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Brewery getOne(@PathVariable int id){
        try{
            return service.getOne(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void create(@RequestBody @Valid Brewery brewery){
        try{
            service.create(brewery);
        }catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping("/{id")
    public void delete(@PathVariable int id){
        try{
            service.delete(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

}
