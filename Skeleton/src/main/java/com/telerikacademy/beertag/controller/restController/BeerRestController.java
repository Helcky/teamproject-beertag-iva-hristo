package com.telerikacademy.beertag.controller.restController;

import com.telerikacademy.beertag.exceptions.DuplicateEntityException;
import com.telerikacademy.beertag.exceptions.EntityNotFoundException;
import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.services.common.BeerService;
import com.telerikacademy.beertag.services.common.BreweryService;
import com.telerikacademy.beertag.services.common.StyleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {

    private BeerService beerService;
    private StyleService styleService;
    private BreweryService breweryService;
   // private TagService tagService;

    public BeerRestController(BeerService beerService, StyleService styleService, BreweryService breweryService){
        this.beerService =beerService;
        this.styleService= styleService;
        this.breweryService=breweryService;
        //this.tagService=tagService;
    }

    @GetMapping
    public List<Beer> getAll(){
        return beerService.getAll();
    }

    @GetMapping("/{id}")
    public Beer getOne(@PathVariable int id){
        try{
            return beerService.getOne(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void create(@RequestBody @Valid BeerDTO beerDTO){
        try{
            beerService.create(mapperCreate(beerDTO));
        }catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try{
        beerService.delete(id);
    }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public void update(@PathVariable int id, @RequestBody @Valid BeerDTO beerDTO){
        try{
             beerService.update(mapperUpdate(id, beerDTO));
        }catch(EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{beer_id}/add/{tag_id}")
    public void addTagToBeer(@PathVariable int beer_id,@PathVariable int tag_id){
        try{
            beerService.addTagToBeer(beer_id, tag_id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }catch (DuplicateEntityException d){
            throw new ResponseStatusException(HttpStatus.CONFLICT,d.getMessage());
        }

    }
//
//    @GetMapping("/filter/style/{style_id}")
//    public List<Beer> beersFilterByStyle(@PathVariable int style_id){
//        try{
//           return beerService.filterByStyle(style_id);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//
//    @GetMapping("/filter/tag/{tag_id}")
//    public List<Beer> beersFilterByTag(@PathVariable int tag_id){
//        try{
//            return beerService.filterByTag(tag_id);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }
//
//    @GetMapping("/filter/country/{country_id}")
//    public List<Beer> beersFilterByCountry(@PathVariable int country_id){
//        try{
//            return beerService.filterByCountry(country_id);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
//        }
//    }


    @GetMapping("/sort/abv")
    public List<Beer> sortByABV(){
        return beerService.sortByABV();
    }

    @GetMapping("/sort/name")
    public List<Beer> sortByName(){
        return beerService.sortByName();
    }

    private Beer mapperUpdate(int id, BeerDTO beerDTO){
        try{
            Beer newBeer=beerService.getOne(id);
            return mainMapper(newBeer,beerDTO);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }
    private Beer mapperCreate(BeerDTO beerDTO){
        try{
            Beer newBeer=new Beer();
            return mainMapper(newBeer,beerDTO);
            //TODO tags
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private Beer mainMapper(Beer newBeer, BeerDTO beerDTO) {
        try {
            newBeer.setName(beerDTO.getName());
            newBeer.setDescription(beerDTO.getDescription());
            newBeer.setAbv(beerDTO.getAbv());
            Style style = styleService.getOne(beerDTO.getStyleID());
            newBeer.setStyle(style);
            Brewery brewery = breweryService.getOne(beerDTO.getBreweryID());
            newBeer.setBrewery(brewery);
            return newBeer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
